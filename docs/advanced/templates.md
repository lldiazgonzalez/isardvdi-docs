# Templates

Templates is the key in IsardVDI quick deployment system. Here advanced users can manage their templates and decide with whom they share it or not.

## Create template

On destops list, when a desktop is stopped you will find in its details (clicking the + button on the left) a button to **Template it**. The form to create a template will ask for some information:

![](../images/desktops/desktop-template-form.png)

- **Name**: Give it a name that users will search for when creating a new desktop from that template.
- **Description**: Optional but will give users information about.
- **Kind**: Only admin roles will see this option that lets choose where the new template will be stores, either in bases folder or in templates folder.
- **Allows**:  If you open the [alloweds](allows.md#allows-form) section you will be able to decide which roles, categories, groups and individual users you want to share this template. That means who will be able to find this template when creating a new desktop. By default all options are unchecked, what means that no one will see this template. If you add a full role, all the categories in that role will see that template. The same with groups and users.
- **Hardware**: You can now set the default hardware that this template will need. Users will be able to modify that hardware when creating their desktops based on this template but within their [quota](../user/quotas.md).

When you click on create template button two main actions will happen:

1. Your desktop disk will be moved to the new template thus deleting your current desktop.
2. A new desktop from the new template will be created identical to the one you had before creating the template. All the changes you do from now on to that desktop are not connected to the template anymore.

Now all the users whom you shared that template will be able to create a desktop identical to that template in the moment of the creation.

**<u>ALERT</u>**: <u>The process of creating a template will create an EXACT COPY of the source desktop. That means that whatever you have stored in that desktop will also be replicated in that template, and users that create desktops from that template WILL HAVE AN EXACT COPY of that desktop. Check twice before creating a template that the browsers inside don't cached your email accounts and that you have not stored any personal information in the desktop.</u>


## Shares

On template list, cliking the icon of Shares column can be set to whom in the system the template is going to be shared based on [allows](allows.md#allows-form).

## Template details

On template list, when you click on the **+** sign to the left of each template a details view will open. There you will find actions and information about template hardware being used and system status information.

