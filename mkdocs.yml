docs_dir: docs

# Project information
site_name: IsardVDI
site_url: http://www.isardvdi.com
site_description: IsardVDI Open Source Virtual Desktops.
site_author: IsardVDI

# Repository
repo_url: https://gitlab.com/isard/isardvdi-docs

# Copyright
copyright: Copyright &copy; 2016 <a href="http://www.isardvdi.com">IsardVDI</a>.

# Documentation and theme
theme:
  name: 'material'
  logo: 'images/logo.svg'
  features:
    - toc.integrate

extra_css:
    - css/extra.css

# Options
extra:
  palette:
    primary: 'indigo'
    accent: 'indigo'
  font:
    text: 'Roboto'
    code: 'Roboto Mono'

# Google Analytics
#google_analytics:
#  - 'UA-XXXXXXXX-X'
#  - 'auto'

# Extensions
markdown_extensions:
  - admonition
  - codehilite:
      guess_lang: false
  - toc:
      permalink: true

plugins:
  - search
  - i18n:
      languages:
        ca: "Català"
        es: "Castellano"
        en: "English"
      default_language: 'en'
      no_translation:
        ca: "Aquesta pàgina no està traduida al català."
        en: "This page isn't translated to English."
        es: "Esta página no está traducida al castellano."
      translate_nav:
        ca:
          user: "Usuari"
          advanced: "Avançat"
          manager: "Mànager"
          administrator: "Administrador"
          installation: "Instaŀlació"
          deploy: "Desplegament"
        en:
          user: "User"
          advanced: "Advanced"
          manager: 'Manager'
          administrator: "Administrador"
          installation: "Installation"
          deploy: "Deploy"
        es:
          user: 'Usuario'
          advanced: 'Avanzado'
          manager: 'Mánager'
          administrator: 'Administador'
          installation: "Instalación"
          deploy: "Despliegue"

# TOC
nav:
- index.md
- user:
    - user/index.md
    - user/webinterfaces.md
    - user/local-client.md
    - user/desktops.md
    - user/quotas.md
- advanced:
    - advanced/index.md
    - advanced/templates.md
    - advanced/media.md
    - advanced/allows.md
- manager:
    - manager/index.md
    - manager/first_steps.ca.md
    - manager/first_steps.es.md
    - Users: manager/users.md
    - Desktops: manager/domains/desktops.md
    - Media: manager/domains/media.md
- administrator:
    - admin/index.md
    - admin/entities.md
    - admin/multitenant.md
    - admin/hypervisors.md
    - admin/updates.md
    - FAQ: admin/faq.md
- installation:
    - install/index.md
    - install/install.md
    - install/wizard.md
    - install/first-steps.md
    - install/certificates.md
    - extras/grafana.md
- deploy:
    - Introduction: deploy/index.md
    - Storage:
        - deploy/storage/concepts.md
        - deploy/storage/raid.md
        - deploy/storage/drbd.md
        - Cache:
            - EiO: deploy/storage/cache/eio.md
            - Writeboost: deploy/storage/cache/writeboost.md
    - Networking:
        - Linux commands: deploy/networking/concepts.md
    - Clusters & HA:
        - Pacemaker: deploy/clusters/pacemaker.md
        - OCF resources:
            - deploy/clusters/ocf/eio.md
            - deploy/clusters/ocf/writeboost.md
        - deploy/clusters/live-migration.md
    - Setups:
        - HA Clusters:
            - Active-Pasive: deploy/setups/ha/active_passive.md
            - Active-Active: deploy/setups/ha/active_active.md
        - Virtualization:
            - GPU SRIOV: deploy/setups/virtualization/gpu_sriov.md
    - Utilities:
        - Monitoring: deploy/utilities/monitoring.md
        - Grafana: deploy/utilities/grafana.md
        - deploy/utilities/fio.md
